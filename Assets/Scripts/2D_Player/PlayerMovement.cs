using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private BoxCollider2D coll;
    private float directionX;
    private float walkSpeed = 7f;

    public bool isWallSliding;
    private float wallSlidingSpeed = 2f;
    [SerializeField] private Transform wallCheck;
    [SerializeField] private LayerMask wallLayer;

    private bool isWallJumping;
    private float wallJumpingDirection;
    private float wallJumpingTime = 0.2f;
    private float wallJumpingCounter;
    private float wallJumpingDuration = 0.4f;
    private Vector2 wallJumpingPower = new Vector2(7f, 10f);

    [SerializeField] private LayerMask jumpableGround;
    public int extraJumps;
    private int originalAmountOfJumps;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        coll = GetComponent<BoxCollider2D>();
        originalAmountOfJumps = extraJumps;
    }

    // Update is called once per frame
    void Update()
    {
        directionX = Input.GetAxisRaw("Horizontal");

        // checks if the player is on the ground
        // applies and upward velocity if the player can jump
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x,12f);
        } 
        // If extra jumps is set to more than 0, allow another jump
        else if (Input.GetButtonDown("Jump") && !IsGrounded() && CanJumpAgain() && !isWallJumping )
        {
            rb.velocity = new Vector2(rb.velocity.x, 12f);
            extraJumps--;
        }

        if (!isWallJumping)
        {
            // These checks are to make sure the player faces the direction of movement
            if (directionX < 0)
            {
                gameObject.transform.localScale = new Vector3(-1f, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
            }
            else if (directionX > 0)
            {
                gameObject.transform.localScale = new Vector3(1f, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
            }
        }
        

        WallSlide();
        WallJump();
        
    }

    private void FixedUpdate()
    {
        if (!isWallJumping)
        {
            rb.velocity = new Vector2(directionX * walkSpeed, rb.velocity.y);
        }    
    }

    // checks slightly below the feet of the player to see if there is ground directly below them
    private bool IsGrounded()
    {
        if (Physics2D.BoxCast(coll.bounds.center, coll.bounds.size, 0f, Vector2.down, .1f, jumpableGround))
        {
            extraJumps = originalAmountOfJumps;
            return true;
        }
        return false;
        
    }

    private bool CanJumpAgain()
    {
        return extraJumps > 0;
    }

    private bool IsWalled()
    {
        return Physics2D.OverlapCircle(wallCheck.position, 0.2f, wallLayer);
    }
    
    private void WallSlide()
    {
        if (IsWalled() && !IsGrounded() && directionX != 0f)
        {
            isWallSliding = true;
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y, -wallSlidingSpeed, float.MaxValue));
        }
        else
        {
            isWallSliding = false;
        }
    }

    private void WallJump()
    {
        if (isWallSliding)
        {
            isWallJumping = false;
            wallJumpingDirection = -transform.localScale.x;
            wallJumpingCounter = wallJumpingTime;

            CancelInvoke(nameof(StopWallJumping));
        }
        else
        {
            wallJumpingCounter -= Time.deltaTime;
        }

        if(Input.GetButtonDown("Jump") && wallJumpingCounter > 0f)
        {
            isWallJumping = true;
            rb.velocity = new Vector2(wallJumpingDirection * wallJumpingPower.x, wallJumpingPower.y);
            wallJumpingCounter = 0f;

            if (transform.localScale.x != wallJumpingDirection)
            {
                Vector3 localScale = transform.localScale;
                localScale.x *= -1f;
                transform.localScale = localScale;
            }

            Invoke(nameof(StopWallJumping), wallJumpingDuration);
        }
    }

    private void StopWallJumping()
    {
        isWallJumping = false;
    }
}
