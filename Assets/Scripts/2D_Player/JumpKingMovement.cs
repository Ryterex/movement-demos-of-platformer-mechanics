using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpKingMovement : MonoBehaviour
{
    public float walkSpeed;
    private float moveInput;
    public bool isGrounded;
    private Rigidbody2D rb;
    public LayerMask groundMask;
    public bool canJump = true;
    public float jumpValue = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = Input.GetAxisRaw("Horizontal");

        if (jumpValue == 0.0f && isGrounded)
        {
            rb.velocity = new Vector2(moveInput * walkSpeed, rb.velocity.y);
        }
        

        isGrounded = Physics2D.OverlapBox(new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.5f), new Vector2(0.9f, 0.4f), 0f, groundMask);

        if(Input.GetKey("space") && isGrounded && canJump)
        {
            jumpValue += 0.05f;
        }

        if (Input.GetKeyDown("space") && isGrounded && canJump)
        {
            rb.velocity = new Vector2(0.0f, rb.velocity.y);
        }

        if (jumpValue >= 20f && isGrounded)
        {
            float tempX = moveInput * walkSpeed;
            float tempY = jumpValue;
            rb.velocity = new Vector2(tempX, tempY);
            Invoke("ResetJump", 0.2f);
        }

        if (Input.GetKeyUp("space"))
        {
            if (isGrounded)
            {
                rb.velocity = new Vector2(moveInput * walkSpeed, jumpValue);
                jumpValue = 0.0f;
            }
        }

        if (isGrounded)
        {
            canJump = true;
        }

        // These checks are to make sure the player faces the direction of movement
        if (moveInput < 0)
        {
            gameObject.transform.localScale = new Vector3(-1f, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        }
        else if (moveInput > 0)
        {
            gameObject.transform.localScale = new Vector3(1f, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        }
    }

    private void ResetJump()
    {
        canJump = false;
        jumpValue = 0;
    }
}
