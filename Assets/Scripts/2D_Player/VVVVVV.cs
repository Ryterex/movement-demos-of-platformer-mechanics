using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VVVVVV : MonoBehaviour
{
    public bool isGrounded = false;
    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float vertInput = Input.GetAxisRaw("Vertical");

        if (isGrounded)
        {
            if(horizontalInput + vertInput != 0)
            {
                if (horizontalInput > 0)
                {
                    Physics2D.gravity = new Vector2(9.8f, 0f);
                }
                else if (horizontalInput < 0)
                {
                    Physics2D.gravity = new Vector2(-9.8f, 0f);
                }
                else if (vertInput > 0)
                {
                    Physics2D.gravity = new Vector2(0, 9.8f);
                }
                else
                {
                    Physics2D.gravity = new Vector2(0f, -9.8f);
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }
}
