Hello!

This is Movement Demos of Platformer Mechanics.

This repo is designed to showcase some of the movement types demonstrated within a platformer video game.

The goal of this repo is to congregate the information found all over the internet and put it all in one place to help other developers and designers with creating their own games. The project can be downloaded and worked with as long as you have the Unity Editor. There will be periodic updates to this repo as well as new information is gathered.

For now, please take a look at this prototype.

Credits for Mechanics replicated through code:

Basement Experiments (2020, January 12). How to change Gravity Direction in 1 minute | Quick Unity Tutorial [Video]. Youtube. https://www.youtube.com/watch?v=KV8wNSo91bM  

Bendux (2021, April 22). How To Make 2D Ladders In Unity [Video]. Youtube. https://www.youtube.com/watch?v=yyg0yV2roPk 

Bendux (2022, October 26). How To Wall Slide & Wall Jump in Unity [Video]. Youtube. https://www.youtube.com/watch?v=O6VX6Ro7EtA 

Outrage (2020, May 1). I Mdea Jump King in Unity (How to easily do it) [Video]. Youtube. https://www.youtube.com/watch?v=o4Z99Xi5xXc  
